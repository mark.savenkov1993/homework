
/*1) Необходимо реализовать функцию, печатающую все
простые числа из заданного диапазона.*/

function showSimpleNumbers(a, b) {

    a = +prompt('Введите нижний порог диапазона - A', 'Число больше 1');
    b = +prompt('Введите верхний порог диапазона - В', 'Число больше A');

    let arrOfSimpleNumbers = [];

    nextIteration:

    for (let i = a; i <= b; i++) {

        for (let j = 2; j < i; j++) {

            if (i % j == 0) continue nextIteration;
        }

        arrOfSimpleNumbers.push(i);

    }

    return arrOfSimpleNumbers;
}

console.log(showSimpleNumbers());

/*2) Найти сумму цифр заданного числа.*/

function calcSum() {

    let userNumber = prompt('Введите число...', ''),
        sum = 0;

    for (let i = 0; i <= userNumber.length; i++) {
        sum += +userNumber.charAt(i);
    }
    
    return sum;
}

console.log(calcSum());

/*3) Даны натуральные числа n и m. Написать функцию,
которая возвращает результат операции сложения двух
чисел. Первое образовано из k младших цифр числа n,
второе - из k старших цифр числа m.*/

function getSum(a, b) {

    a = +prompt('Введите число n', '');
    b = +prompt('Введите число m', '');

    if (a && b < 1000) {

        console.log('Ваше число меньше 1000');
        return;
    }

    let i = 3,
        k;
  
    while (i !== k) {
    k = +prompt('Введите кол-во знаков - k','');

    if (k === 3) {
      k = 3;
    }
  }

    let x = a % 1000,
        y,
        result;

    console.log(`Это x ${x}`);

    for (let i = b; i >= 1000; i--) {

        y = parseInt(b / 10);

    }

    console.log(`Это у ${y}`);

    result = x + y;

    return result;
}

console.log(getSum());
